<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function downloadCueSheet() {

        $jsonFile      =    'cue_json2.json';
        $fileLocation  =    asset("cue_sheet_json/".$jsonFile);
        $userName      =    'Jerzy';
        $routeModel                 =   new \stdClass;
        $userInfo                   =   new \stdClass;
        $userInfo->first_name       =   $userName;
        $userInfo->last_name        =   'Lamot';
        $routeModel->distance       =   3.1;
        $routeModel->route_name     =   'Near my home';
        $routeModel->vertical_gain  =   2.1;
        $routeModel->cue_sheet_creator  =   $userName;

        $jsonData      =    file_get_contents($fileLocation);
        $cueSheetData  =    json_decode($jsonData, true);

        // return view("cue_sheet_pdf_new",compact(['cueSheetData','userName','routeModel','userInfo']));

        $pdf           =    PDF::loadView("cue_sheet_pdf_new",compact(['cueSheetData','userName','routeModel','userInfo']));
        $pdf->setPaper([0,0,612,792],'portrait');
        $pdf->setOptions([ 'defaultFont' => 'sans-serif']);

        return $pdf->download('route_name'.'.pdf');

    }
}
